# Api para previsao do tempo assíncrona

## MS1 (Microservice 1)

API que recebe o request da previsão do tempo para alguma cidade

## MS2 (Microservice 2)

Cliente que aguarda a solicitação vinda pelo broker para e requisita a 
previsão do tempo do site Openwheather

## Executando o ambiente

É necessário ter o Docker e docker-compose instalado na máquina.

Para executar basta o comando:

    $ docker-compose up

Quando todos os containers estiverem no ar só fazer as chamadas para a url: http://localhost:8000/weather?city=<nome_da_cidade>

## Explicando a stack

### Redis

Banco de dados chave:valor que facilita a utilização do método pub-sub, utilizado como broker.

Banco de dados 0 (zero) utilizado para broker, banco 1 (um) utilizado para dados de retorno.

### Worker

Microserviço que escuta um broker, faz a requisição para previsão do tempo e retorna através do broker o resultado

### httpapi

Api que recebe a requisição http e envia a solicitação ao worker através do broker

## Escolhas feitas no projeto

Redis - Banco fácil de se trabalhar, rápido para o que o projeto se propunha;

Serve tanto para broker com o sistema de publish e subscribe, quando para salvar os dados de retorno. Cheguei a pensar em colocar o banco PostgresSQL que é completo, mas não enxerguei necessidade real para ecolher esta solução.

Flask - Framework simples e rápido para montar rápidas interfaces http

Docker - Melhor maneira para se colocar algum ambiente no ar, onde conseguimos definir toda a parametrização sem necessitar se preocupar com difernças de ambiente

Python Threads - Não necessita mais que isto para conseguir escutar as respostas deste projeto assincronamente, levando em consideração seu tamanho e propósito. Caso tivéssemos necessidades maiores de resposta e números de transações, talvez fosse uma opção algum sistema distribuiído e escalável como Celery por exemplo.

Configurações necessárias para o funcionamento todas em variáveis de ambiente setadas no docker



