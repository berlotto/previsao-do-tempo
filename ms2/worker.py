"""
Simple subscribe service that listening on a redis broker
to make a weather request and return the result between messages.

To run:
pipenv run python worker.py
"""
import os

import requests
import simplejson as json

import redis

DB_HOST=os.getenv("DB_HOST", 'localhost')
DB_PORT=os.getenv("DB_PORT", 6379)

r = redis.StrictRedis(host=DB_HOST, port=DB_PORT, db=0)
API_KEY = os.getenv('API_KEY')

handle = r.pubsub(ignore_subscribe_messages=True)
handle.subscribe('weather_request')

print("Waiting for messages...")

for message in handle.listen():
    print(message)
    
    request_data = json.loads(message['data'])
    
    response_key = request_data['key']
    city_name = request_data['city']

    url = f"https://api.openweathermap.org/data/2.5/weather?q={city_name}&lang=pt&units=metric&appid={API_KEY}"
    weather_request = requests.get(url)

    r.publish(f'weather_response_{response_key}', json.dumps(weather_request.json()) )
