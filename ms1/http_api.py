"""
Simple flask api application to respond HTTP request with
the current weather of the desired city:

To Run: 
$ export FLASK_APP=http_api.py
$ pipenv run flask run
or 
$ FLASK_DEBUG=True pipenv run flask run

To call the api:
$ curl -X GET http://localhost:8000/weather?city=Joinville
"""
import random as rand
from threading import Thread
import time
import os

import simplejson as json

import redis
from flask import Flask, jsonify, request

app = Flask(__name__)

DB_HOST=os.getenv("DB_HOST", 'localhost')
DB_PORT=os.getenv("DB_PORT", 6379)

r = redis.StrictRedis(host=DB_HOST, port=DB_PORT, db=0)

db = redis.Redis(host=DB_HOST, port=DB_PORT, db=1)

def save_response_to_database(city_name, weather_data):
    global db
    db.set(city_name, json.dumps(weather_data))

def wait_for_response(city_name, response_key):
    # Wait for the response on the key...
    global r
    handle = r.pubsub(ignore_subscribe_messages=True)

    broker_queue_name = f'weather_response_{response_key}'
    handle.subscribe(broker_queue_name)

    async_wheather = None
    times = 0
    while not async_wheather and times < 10:
        async_wheather = handle.get_message()
        times += 1
        time.sleep(1)
    
    handle.unsubscribe(broker_queue_name)
    app.logger.info("Response received for {0}".format(city_name))
    app.logger.debug(async_wheather)

    if async_wheather:
        save_response_to_database(city_name, async_wheather['data'])

@app.route('/requestweather', methods=("GET",))
def get_async_weather():
    city_name = request.args.get("city", None)

    if not city_name:
        return 'Need to specify the city!\n'
    
    # Send the request for the broker
    
    response_key = str(rand.getrandbits(120))

    request_data = {
        'key': response_key,
        'city': city_name
    }
    r.publish('weather_request', json.dumps(request_data))

    t = Thread(target=wait_for_response, args=(city_name, response_key,))
    t.start()

    return jsonify({'message':"Your request will saved in the database! Thankyou"})

@app.route('/getweather', methods=("GET",))
def get_db_weather():
    city_name = request.args.get("city", None)

    if not city_name:
        return jsonify({'error':'Need to specify the city'})

    str_weather = db.get(city_name)
    if not str_weather:
        return jsonify({'error':'Sorry, no whether data found. Please make a request'})
    else:
        json_weather = json.loads(json.loads(str(str_weather,'utf-8')))
        
        return jsonify(json_weather)
